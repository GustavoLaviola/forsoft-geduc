﻿using geducSite.Context;
using geducSite.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace geducSite.Persistence
{
    public class CargoDAO : Conexao
    {
        public void Cadastrar(Cargo c)
        {
            try
            {
                AbrirConexao();

                myCmd = new MySqlCommand("insert into cargo(cargo,funcao) values (@v1,@v2)", myCon);
                myCmd.Parameters.AddWithValue("@v1", c.cargo);
                myCmd.Parameters.AddWithValue("@v2", c.funcao);
                myCmd.ExecuteNonQuery();

                cmd = new SqlCommand("insert into cargo(cargo,funcao) values (@v1,@v2)", con);
                cmd.Parameters.AddWithValue("@v1",c.cargo);
                cmd.Parameters.AddWithValue("@v2", c.funcao);
                cmd.ExecuteNonQuery();


            }
            catch
            {
                throw;
            }
            finally
            {
                FecharConexao();
            }
        }

        public IEnumerable<Cargo> Listar()
        {
            try
            {
                AbrirConexao();
                Collection<Cargo> retorno = new Collection<Cargo>();

                cmd = new SqlCommand("select * from cargo", con);
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    var c = new Cargo();
                    c.idCargo = Convert.ToInt32(dr["idCargo"]);
                    c.cargo = Convert.ToString(dr["cargo"]);
                    c.funcao = Convert.ToString(dr["funcao"]);
                    retorno.Add(c);
                }
                return retorno;
            }
            finally
            {
                FecharConexao();

            }

        }
    }
}