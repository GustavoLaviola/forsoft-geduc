﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/layout.Master" AutoEventWireup="true" CodeBehind="listar_funcionario.aspx.cs" Inherits="geducSite.View.listar_funcionario" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Conteudo" runat="server">
    <table class="table table-bordered table-striped table-hover table-heading table-datatable">
            <tr >
                <th>Nome</th>
                <th>Situacao</th> 
                <th>Sexo</th>   
                <th>Data de nascimento</th>   
                <th>Telefone</th>  
                <th>Celular</th>
                <th>CPF</th>    
                <th>RG</th> 
            </tr>
        <%foreach (var lista in ListarFuncionaio())
          {%>
            <tr>
                <td><%: lista.nome %></></td>
                <td><%: lista.situacao %></td>
                <td><%: lista.sexo %></td>
                <td><%: lista.dataNascimento %></td>
                <td><%: lista.contato.telefoneFixo %></td>
                <td><%: lista.contato.telefoneCelular %></td>
                <td><%: lista.documento.cpf %></td>
                <td><%: lista.documento.rg %></td>
            </tr>
            <%} %>

    </table>
</asp:Content>
