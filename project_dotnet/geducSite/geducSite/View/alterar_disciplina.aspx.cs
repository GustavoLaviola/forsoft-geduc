﻿using geducSite.Models;
using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace geducSite.View
{
    public partial class altear_disciplina : System.Web.UI.Page
    {

        protected IEnumerable<Disciplina> todasAsDisciplina;
        protected void Page_Load(object sender, EventArgs e)
        {
            /*
            geducSite.Models.Login login = (geducSite.Models.Login)Session["login"];

            if (login == null)
            {
                if (Request.Cookies["login"] != null)
                {
                    HttpCookie coockie = Request.Cookies["login"];
                    string ck = coockie.Value.ToString();

                    Session["login"] = new UtilDAO().SessionLogin(ck);
                }
                else
                {
                    Response.Redirect("http://www.projetogeduc.com.br:8080/geduc/login.jsp");
                }
            }
            else if (Util.ValidarAcesso(login.perfilAcesso, "alterar_disciplina.aspx") == false)
            {
                Response.Redirect("http://www.projetojeduc.com.br:8080/geduc/nao_pode_acessar.jsp");
            }
             * */
            //fim session loginss


            if (Session["ListaFuncionarios"] != null)
            {
                todasAsDisciplina = (IEnumerable<Disciplina>)Session["todasAsDisciplina"];
            }
            else
            {
                todasAsDisciplina = new DisciplinaDAO().ListarDisciplinas();
                Session["todasAsDisciplina"] = todasAsDisciplina;
                Session.Timeout = 6000;
            }
        }

        protected void btnAlterar_Click(object sender, EventArgs e)
        {
            msg.InnerText = String.Empty;
            String[] campos = new String[] { txtNomeDisciplina.Text, txtCodigoDisciplina.Text, txtCargaHorariaDisciplina.Text };

            if (Validar.tamanhoMax(txtNomeDisciplina.Text, 40) && Validar.tamanhoMax(txtCodigoDisciplina.Text, 20))
            {
                if (Validar.seSomenteNumero(txtCargaHorariaDisciplina.Text))
                {
                    Disciplina dis = todasAsDisciplina.SingleOrDefault(x => x.codigo == txtCodigoDisciplina.Text);
                    if (dis != null)
                    {
                        dis.nome = txtNomeDisciplina.Text;
                        dis.codigo = txtCodigoDisciplina.Text;
                        dis.cargaHoraria = Convert.ToInt32(txtCargaHorariaDisciplina.Text);

                        new DisciplinaDAO().Atualizar(dis);

                        limpar();

                        msg.InnerText = "Alterado com sucesso!!!";
                    }
                    else msg.InnerText = "Disciplina não encontrada";
                }
                else msg.InnerText = "Preenchimento de campo inválido";
            }
            else msg.InnerText = "Ultrapassou o limite máximo de caracteres";
        }

        public void limpar()
        {
            txtNomeDisciplina.Text = string.Empty;
            txtCodigoDisciplina.Text = string.Empty;
            txtCargaHorariaDisciplina.Text = string.Empty;
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            Disciplina d = todasAsDisciplina.SingleOrDefault(x => x.codigo == txtCodigoDisciplina.Text);
            if (d != null)
            {
                txtNomeDisciplina.Text = d.nome;
                txtCargaHorariaDisciplina.Text = Convert.ToString(d.cargaHoraria);
            }
        }
    }
}