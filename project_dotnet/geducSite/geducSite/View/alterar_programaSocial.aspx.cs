﻿using geducSite.Models;
using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace geducSite.View
{
    public partial class alterar_programaSocial : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        { 
        }

        protected void btnAlterarProgSoci_Click(object sender, EventArgs e)
        {
            try
            {
                var programa = new ProgramaSocialDAO().listar();
                ProgramaSocial p = programa.SingleOrDefault(x => x.nomePrograma == txtbusca.Text);
                if (p != null)
                {
                    p.nomePrograma = txtNome.Text;
                    p.descricao = txtDescricao.Text;
                    p.ambitoAdm = txtAmbito.Text;
                    new ProgramaSocialDAO().Alterar(p);
                    msg.Text = "Alterado com sucesso";
                }
                else {
                    msg.Text = "Programa não encontrado";
                }
            }
            catch (Exception erro)
            {
                msg.Text = erro.Message;
            }
        }

        protected void verificar_Click(object sender, EventArgs e)
        {
            var programa = new ProgramaSocialDAO().listar();
            ProgramaSocial p = programa.SingleOrDefault(x => x.nomePrograma == txtbusca.Text);
            if (p != null)
            {
                txtNome.Text = p.nomePrograma;
                txtDescricao.Text= p.descricao ;
                txtAmbito.Text = p.ambitoAdm;
            }
            else
            {
                msg.Text = "Programa não encontrado";
            }
                   
        }
    }
}