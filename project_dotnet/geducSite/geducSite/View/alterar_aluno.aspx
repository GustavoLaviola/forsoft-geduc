﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/layout.Master" AutoEventWireup="true" CodeBehind="alterar_aluno.aspx.cs" Inherits="geducSite.View.alterar_aluno" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Conteudo" runat="server">
    
 
   <div class="row">
            <div class="col-sm-8">
                <div class="box-content"> 
    <form id="form1" runat="server">

        <!-- Início do Código -->
        <a href="alterar_aluno.aspx">alterar_aluno.aspx</a>
        <fieldset>
            <legend class="text-center">Alteração de Aluno</legend>
            <asp:Label ID="msg" runat="server"></asp:Label>

            <asp:Label Text="Buscar" runat="server" />
            
            <div class="form-group">
                <asp:TextBox ID="txtBuscar" runat="server"  />
                <div class="col-sm-4">
                <asp:DropDownList ID="ddlTipoDeBusca" CssClass="form-control" runat="server">
                    <asp:ListItem Value="2">Matricula</asp:ListItem>
                    <asp:ListItem Value="1">CPF</asp:ListItem>
                </asp:DropDownList>
                <asp:Button ID="btnBuscar" runat="server"  Text="Buscar" OnClick="btnBuscar_Click" />
                </div>
            </div>
        </fieldset>

        <fieldset>
            <legend class="text-center">Login</legend>
            <div class="form-group">
                <asp:Label ID="lblNomeUsuario" runat="server" Text="Usu&aacute;rio:" CssClass="col-sm-2"></asp:Label>
                <div class="col-sm-4">
                    <asp:TextBox ID="txtUsuario" name="lblNomeUsuario" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                
                <asp:Label ID="lblSenha" runat="server" Text="Senha:" CssClass="col-sm-2"></asp:Label>
                <div class="col-sm-4">
                    <asp:TextBox ID="txtSenha" name="lblSenha" runat="server" CssClass="form-control"></asp:TextBox>
                </div>

            </div>
        </fieldset>
        
        <fieldset>
            <legend class="text-center">Dados Pessoais</legend>

            <div class="form-group">
                <asp:Label ID="lblNomeAluno" runat="server" Text="Nome:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-6">
                    <asp:TextBox ID="txtNomeAluno" name="lblNomeAluno" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                
                <asp:Label ID="lblDataNascimento" runat="server" Text="Data de Nascimento:" CssClass="col-sm-2"></asp:Label>
                <div class="col-sm-4">
                    <asp:TextBox ID="txtDataNascimento" name="lblDataNascimento" runat="server" CssClass="form-control" TextMode="DateTime"></asp:TextBox>
                </div>
            </div>

        <div class="form-group">
            <asp:Label ID="lblSexo" runat="server" Text="Sexo:" CssClass="col-sm-2 control-label"></asp:Label>
            <div class="col-sm-6">
                <asp:RadioButtonList ID="rbSexo" runat="server">
                    <asp:ListItem Value="Masculino" Text="Masculino" Selected="True" />
                    <asp:ListItem Value="Feminino" Text="Feminino" />
                </asp:RadioButtonList>
            </div>
        </div>
            
        <div class="form-group">
            <asp:Label ID="lblNaturalidade" runat="server" Text="Naturalidade:" CssClass="col-sm-2 control-label"></asp:Label>
            <div class="col-sm-4">
                <asp:TextBox ID="txtNaturalidade" name="lblNaturalidade" runat="server" CssClass="form-control"></asp:TextBox>
            </div>

            <asp:Label ID="lblNacionalidade" runat="server" Text="Nacionalidade:" CssClass="col-sm-2 control-label"></asp:Label>
            <div class="col-sm-4">
                <asp:TextBox ID="txtNacionalidade" name="lblNacionalidade" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>

        <div class="form-group">        
            <asp:Label ID="lblNomePai" runat="server" Text="Nome do Pai:" ></asp:Label>
            <asp:TextBox ID="txtNomePai" name="lblNomePai" runat="server" ></asp:TextBox>


            <asp:Label ID="lblNomeMae" runat="server" Text="Nome da M&atilde;e:" ></asp:Label>
            <asp:TextBox ID="txtNomeMae" name="lblNomeMae" runat="server" ></asp:TextBox>
        </div>

        <div class="form-group">

            <asp:Label ID="lblEtnia" runat="server" Text="Etnia:" CssClass="col-sm-2 control-label"></asp:Label>
            <div class="col-sm-4">
                <asp:DropDownList ID="ddlEtnia" name="lblEtnia" runat="server" CssClass="form-control">
                    <asp:ListItem Value="Branco">Branco(a)</asp:ListItem>
                    <asp:ListItem Value="Negro">Negro(a)</asp:ListItem>
                    <asp:ListItem Value="Amarelo">Amarelo(a)</asp:ListItem>
                    <asp:ListItem Value="Pardo">Pardo</asp:ListItem>
                    <asp:ListItem Value="Indigena">Ind&iacute;gena</asp:ListItem>
                    <asp:ListItem Value="Sem Declaracao">Sem Declara&ccedil;&atilde;o</asp:ListItem>
                </asp:DropDownList>
            </div>


            <asp:Label ID="lblEstadoCivil" runat="server" Text="Estado Civil:" CssClass="col-sm-2 control-label"></asp:Label>
            <div class="col-sm-4">
                <asp:DropDownList ID="ddlEstadoCivil" name="lblEstadoCivil" runat="server" CssClass="form-control">
                    <asp:ListItem Value="Solteiro">Solteiro (a)</asp:ListItem>
                    <asp:ListItem Value="Casado">Casado (a)</asp:ListItem>
                    <asp:ListItem Value="Divorciado">Divorciado (a)</asp:ListItem>
                    <asp:ListItem Value="Viuvo">Vi&uacute;vo (a)</asp:ListItem>
                    <asp:ListItem Value="Sem Declaracao">Sem Declara&ccedil;&atilde;o</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class="form-group">
            <asp:Label ID="lblEscolaridade" runat="server" Text="N&iacute;vel de Escolaridade:" CssClass="col-sm-2 control-label"></asp:Label>
            <div class="col-sm-6">
                <asp:DropDownList ID="ddlEscolaridade" name="lblEscolaridade" runat="server" CssClass="form-control">
                    <asp:ListItem Text="Selecione uma Op&ccedil;&atilde;o" Value="selecione" />
                    <asp:ListItem Text="Superior" Value="superior" />
                    <asp:ListItem Text="Superior Incompleto" Value="superioin" />
                    <asp:ListItem Text="T&eacute;cnico" Value="tecnico" />
                    <asp:ListItem Text="T&eacute;cnico Incompleto (a)" Value="tecnicoin" />
                    <asp:ListItem Text="M&eacute;dio" Value="medio" />
                    <asp:ListItem Text="Vi&M&eacute;dio Incompleto" Value="medioin" />
                    <asp:ListItem Text="Fundamental" Value="fundamental" />
                    <asp:ListItem Text="Fundamental Incompleto" Value="fundamentalin" />
                </asp:DropDownList>
            </div>
        </div>

        <div class="form-group">
            <asp:Label ID="lblNecessidadeEspecial" runat="server" Text="Possui alguma necessidade especial?" ></asp:Label>
            <asp:RadioButtonList ID="rbNecessidadeEspecial" runat="server">
                <asp:ListItem Value="Sim" Text="Sim" />
                <asp:ListItem Value="Não" Text="Não" />
            </asp:RadioButtonList>
        </div>
        </fieldset>
        

        
        <fieldset>
            <legend class="text-center">Aluno</legend>
                
            <div class="form-group">
                <asp:Label ID="lblMatricula" runat="server" Text="Matr&iacute;cula:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                    <asp:TextBox ID="txtMatricula" name="lblMatricula" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                </div>                

                <asp:Label ID="lblSituacao" runat="server" Text="Situa&ccedil;&atilde;o:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                    <asp:DropDownList ID="ddlSituacao" name="lblSituacao" runat="server" CssClass="form-control">
                        <asp:ListItem>Escolha uma op&ccedil;&atilde;o</asp:ListItem>
                        <asp:ListItem Value="Ativo">Ativo</asp:ListItem>
                        <asp:ListItem Value="Inativo">Inativo</asp:ListItem>
                    </asp:DropDownList><asp:Label ID="Label15" runat="server" Text="*" CssClass="form-control" />
                </div>
            </div>
        </fieldset>

        
        <fieldset>
            <legend>Documenta&ccedil;ao </legend>
            
            <div class="form-group">
                
                <asp:Label ID="lblCPF" runat="server" Text="CPF:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                    <asp:TextBox ID="txtCPF" name="lblCPF" runat="server" CssClass="form-control"></asp:TextBox>
                </div>

                <asp:Label ID="lblRG" runat="server" Text="RG:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                    <asp:TextBox ID="txtRG" name="lblRG" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>

            <div class="form-group">
                <asp:Label ID="lblExpedido" runat="server" Text="Data de Expedi&ccedil;&atilde;o:" CssClass="col-sm-3 control-label"></asp:Label>
                <div class="col-sm-3">
                    <asp:TextBox ID="txtDataExpedicao" name="lblExpedido" runat="server" CssClass="form-control" TextMode="DateTime"></asp:TextBox>
                </div>


                <asp:Label ID="lblOrgao" runat="server" Text="Org&atilde;o Expedidor:" CssClass="col-sm-3 control-label"></asp:Label>
                <div class="col-sm-3">
                    <asp:TextBox ID="txtOrgao" name="lblOrgao" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </fieldset>

        <fieldset>
            <legend class="text-center">Certid&atilde;o de Nascimento:</legend>
            
            <div class="form-group">
                <asp:Label ID="lblNumCertidaoNascimento" runat="server" Text="N&uacute;mero:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                    <asp:TextBox ID="txtNumCertidaoNascimento" name="lblNumCertidaoNascimento" runat="server" CssClass="form-control"></asp:TextBox>
                </div>

                <asp:Label ID="lblLivroCertidaoNascimento" runat="server" Text="Livro:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm4">
                    <asp:TextBox ID="txtLivroCertidaoNascimento" name="lblLivroCertidaoNascimento" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>

            <div class="form-group">
                <asp:Label ID="lblFolhaCertidaoNascimento" runat="server" Text="Folha:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                    <asp:TextBox ID="txtFolhaCertidaoNascimento" name="lblFolhaCertidaoNascimento" runat="server" CssClass="form-control"></asp:TextBox>
                </div>


                <asp:Label ID="lblDataCertidaoNascimento" runat="server" Text="Data de Emiss&atilde;o:" CssClass="col-sm-2"></asp:Label>
                <div class="col-sm-4">
                    <asp:TextBox ID="txtDataCertidaoNascimento" name="lblDataCertidaoNascimento" runat="server" CssClass="form-control" TextMode="DateTime"></asp:TextBox>
                </div>
            </div>

            <div class="form-group">
                <asp:Label ID="lblTituloEleitor" runat="server" Text="T&iacute;tulo de Eleitor:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                    <asp:TextBox ID="txtTituloEleitor" name="lblTituloEleitor" runat="server" ></asp:TextBox>
                </div>
            </div>
            
            <div class="form-group">
                <asp:Label ID="lblCertificadoReservista" runat="server" Text="Certificado de Reservista:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                <asp:TextBox ID="txtCertificadoReservista" name="lblCertificadoReservista" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </fieldset>

        <fieldset>
            <legend>Endere&ccedil;o </legend>
    
            <div class="form-group">
                <asp:Label ID="lblLogradouro" runat="server" Text="Logradouro:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                    <asp:TextBox ID="txtLogradouro" name="lblLogradouro" runat="server" CssClass="form-control"></asp:TextBox>
                </div>

                <asp:Label ID="lblNumero" runat="server" Text="N&uacute;mero:" CssClass="col-sm-2"></asp:Label>
                <div class="col-sm-4">
                    <asp:TextBox ID="txtNumero" name="lblNumero" runat="server" ></asp:TextBox>
                </div>
            </div>

            <div class="form-group">
                <asp:Label ID="lblComplemento" runat="server" Text="Complemento:" ></asp:Label>
                <asp:TextBox ID="txtComplemento" name="lblComplemento" runat="server" ></asp:TextBox>
            </div>
            
            <div class="form-group">
                <asp:Label ID="lblBairro" runat="server" Text="Bairro:" CssClass="col-sm-1 control-label"></asp:Label>
                <div class="col-sm-3">
                    <asp:TextBox ID="txtBairro" name="lblBairro" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                
                <asp:Label ID="lblCidade" runat="server" Text="Cidade:" CssClass="col-sm-1 control-label"></asp:Label>
                <div class="col-sm-3">
                    <asp:TextBox ID="txtCidade" name="lblCidade" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                
                <asp:Label ID="lblUF" runat="server" Text="UF:" CssClass="col-sm-1 control-label"></asp:Label>
                <div class="col-sm-2">
                    <asp:DropDownList ID="ddlUF" name="lblUF" runat="server" CssClass="form-control">
                        <asp:ListItem Value=" ">  </asp:ListItem>
                        <asp:ListItem Value="AC">AC</asp:ListItem>
                        <asp:ListItem Value="AL">AL</asp:ListItem>
                        <asp:ListItem Value="AP">AP</asp:ListItem>
                        <asp:ListItem Value="AM">AM</asp:ListItem>
                        <asp:ListItem Value="BA">BA</asp:ListItem>
                        <asp:ListItem Value="CE">CE</asp:ListItem>
                        <asp:ListItem Value="DF">DF</asp:ListItem>
                        <asp:ListItem Value="ES">ES</asp:ListItem>
                        <asp:ListItem Value="GO">GO</asp:ListItem>
                        <asp:ListItem Value="MA">MA</asp:ListItem>
                        <asp:ListItem Value="MT">MT</asp:ListItem>
                        <asp:ListItem Value="MS">MS</asp:ListItem>
                        <asp:ListItem Value="MG">MG</asp:ListItem>
                        <asp:ListItem Value="PA">PA</asp:ListItem>
                        <asp:ListItem Value="PB">PB</asp:ListItem>
                        <asp:ListItem Value="PR">PR</asp:ListItem>
                        <asp:ListItem Value="PE">PE</asp:ListItem>
                        <asp:ListItem Value="PI">PI</asp:ListItem>
                        <asp:ListItem Value="RJ">RJ</asp:ListItem>
                        <asp:ListItem Value="RN">RN</asp:ListItem>
                        <asp:ListItem Value="RS">RS</asp:ListItem>
                        <asp:ListItem Value="RO">RO</asp:ListItem>
                        <asp:ListItem Value="RR">RR</asp:ListItem>
                        <asp:ListItem Value="SC">SC</asp:ListItem>
                        <asp:ListItem Value="SP">SP</asp:ListItem>
                        <asp:ListItem Value="SE">SE</asp:ListItem>
                        <asp:ListItem Value="TO">TO</asp:ListItem>
                    </asp:DropDownList>
                </div>                
            </div>

            <div class="form-group">

                <asp:Label ID="lblCEP" runat="server" Text="CEP:" CssClass="col-sm-1 control-label"></asp:Label>
                <div class="col-sm-3">
                    <asp:TextBox ID="txtCEP" name="lblCEP" runat="server" ></asp:TextBox>
                </div>


                <asp:Label ID="lblMunicipio" runat="server" Text="Munic&iacute;pio:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-2">
                    <asp:TextBox ID="txtMunicipio" name="lblMunicipio" runat="server" ></asp:TextBox>
                </div>

                <asp:Label ID="lblZona" runat="server" Text="Zona:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-2">
                    <asp:TextBox ID="txtZona" name="lblZona" runat="server" ></asp:TextBox>
                </div>
            </div>
        </fieldset>

        <fieldset>
            <legend class="text-center">Contato</legend>
            
            <div class="form-group">
            <asp:Label ID="lblTelefone" runat="server" Text="Telefone:" CssClass="col-sm-2 control-label"></asp:Label>
            <div class="col-sm-4">
            <asp:TextBox ID="txtTelefone" name="lblTelefone" runat="server" ></asp:TextBox>
            </div>
            </div>

            <div class="form-group">
            <asp:Label ID="lblCelular" runat="server" Text="Celular:" CssClass="col-sm-2 control-label"></asp:Label>
            <div class="col-sm-4">
            <asp:TextBox ID="txtCelular" name="lblCelular" runat="server" ></asp:TextBox>
            </div>

            <div class="form-group">
            <asp:Label ID="lblEmail" runat="server" Text="E-mail:" CssClass="col-sm-2 control-label"></asp:Label>
            <div class="col-sm-4">
            <asp:TextBox ID="txtEmail" name="lblEmail" runat="server" ></asp:TextBox>
            </div>
            </div>

            <div class="form-group">
            <asp:Label ID="lblOutros" runat="server" Text="Outros:" CssClass="col-sm-2 control-label"></asp:Label>
            <div class="col-sm-4">
            <asp:TextBox ID="txtOutros" name="lblOutros" runat="server" ></asp:TextBox>
            </div>
            </div>
        </fieldset>

        <fieldset>
            <legend class="text-center">Curso</legend>
            <div class="form-group">
                <asp:Label ID="lblCurso" runat="server" Text="Selecione o Curso do aluno:" CssClass="col-sm-2 control-label" /></asp:Label>
                <div class="col-sm-6">
                    <asp:DropDownList ID="ddlCurso" runat="server">            
                    </asp:DropDownList>
                </div>
            </div>
        </fieldset>
        
        <fieldset>
            <legend class="text-center">Programa Social</legend>
            
            <div class="form-group">
                
                <asp:Label ID="lblProgramaSocial" runat="server" Text="Participa de um programa social?" CssClass=".col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                    <asp:RadioButtonList ID="rbProgamaSocial" runat="server" CssClass="form-control">
                        <asp:ListItem Value="Sim" Text="Sim" />
                        <asp:ListItem Value="não" Text="Não" />
                    </asp:RadioButtonList>
                </div>

                <!--"caso o usuario aperte em "sim" devera abrir os seguintes campos: (java script/ jquery" -->
                <asp:Label ID="nomeProgramaSocial" runat="server" Text="Nome:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                    <asp:DropDownList ID="ddlProgramaSocial" name="nomeProgramaSocial" runat="server" CssClass="form-control">
                    </asp:DropDownList>
                </div>
            </div>
        <fieldset>
        <!--<span id="msg"> Mensagem </span> -->

        <asp:Button ID="btnVoltar" runat="server" Text="Voltar" />
        <asp:Button ID="btnLimpar" Text="Limpar" runat="server" OnClick="btnLimpar_Click" />
        <asp:Button ID="btnAlterar" Text="Alterar" runat="server" OnClick="btnAlterar_Click" />

        <!-- Fim do Código -->
    </form>
    </div>
    </div>
    </div>
</asp:Content>
