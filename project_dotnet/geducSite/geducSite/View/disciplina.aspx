﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/layout.Master" AutoEventWireup="true" CodeBehind="disciplina.aspx.cs" Inherits="geducSite.View.disciplina" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Conteudo" runat="server">
    <div class="row">
            <div class="col-sm-8">
                <div class="box-content">
    <form id="form1" runat="server">
        
        <label for="lblId">Identifica&ccedil;&atilde;o</label>
        <asp:DropDownList ID="ddlId" runat="server" />
        <asp:Label ID="lvlID" runat="server" CssClass="alert" Text="Campo destinado a busca e altera&ccedil;&atilde;o" />

        <label for="lblCodigoDisciplina">C&oacute;digo da Disciplina: </label>
        <asp:TextBox ID="txtCodigoDisciplina" runat="server"></asp:TextBox>
        
        <label for="lblNomeDisciplina">Nome da Disciplina: </label>
        <asp:TextBox ID="txtNomeDisciplina" runat="server"></asp:TextBox>
        
        <label for="lblCargaHorariaDisciplina">Carga Hor&aacute;ria: </label>
        <asp:TextBox ID="txtCargaHorariaDisciplina" runat="server" TextMode="Number"></asp:TextBox>
        
        <p><span id="msg" runat="server"></span></p>

        <input type="reset" id="btnLimpar" value="Limpar" />
        
        <asp:Button ID="btnListar" runat="server" Text="Listar" OnClick="btnListar_Click" />
        <asp:Button ID="btnBuscar" runat="server" Text="Buscar" OnClick="btnBuscar_Click" />
        <asp:Button ID="btnAlterar" runat="server" Text="Alterar" OnClick="btnAlterar_Click" />
        <asp:Button ID="btnCadastrar" runat="server" Text="Cadastrar" OnClick="btnCadastrar_Click" />
        <asp:Button ID="btnDeletar" runat="server" Text="Deletar" OnClick="btnDeletar_Click" />


        
        
        <asp:GridView ID="grdDisciplinas" runat="server"></asp:GridView>
    </form>
    </div></div></div>
</asp:Content>
