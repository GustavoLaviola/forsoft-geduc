﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/layout.Master" AutoEventWireup="true" CodeBehind="consultar_nota.aspx.cs" Inherits="geducSite.View.consultar_nota" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Conteudo" runat="server">

<div class="row">
            <div class="col-sm-8">
                <div class="box-content">
    <h1 class="page-header text-center">Consultar Notas</h1>

    <form runat="server">
        <asp:Label Text="Buscar" runat="server" />
        <asp:TextBox ID="txtBuscar" runat="server" />
        <asp:DropDownList runat="server" ID="ddlTipoDeBusca">
            <asp:ListItem Value="1">matricula do aluno</asp:ListItem>
            <asp:ListItem Value="2">nota</asp:ListItem>
            <asp:ListItem Value="3">disciplina</asp:ListItem>
            <asp:ListItem Value="4">data</asp:ListItem>
        </asp:DropDownList>
        <asp:Button ID="btnBuscar" Text="listar" runat="server" OnClick="btnBuscar_Click" />

    </form>

    <div id="DivBusca" runat="server" visible="false">

        <% foreach (var lista in BuscarNotas())
           { %>
           
            <asp:Label runat="server" Text="id:"></asp:Label>
            <label><%: lista.idNota %></label>
            
            <asp:Label runat="server" Text="Nome do Aluno:"></asp:Label>
            <label><%: lista.aluno.nome %></label>
            
            <asp:Label runat="server" Text="Matricula do Aluno:"></asp:Label>
            <label><%: lista.aluno.matricula %></label>
            
            <asp:Label runat="server" Text="Nome da disciplina:"></asp:Label>
            <label><%: lista.disciplina.nome %></label>
            
            <asp:Label runat="server" Text="Codigo da disciplina:"></asp:Label>
            <label><%: lista.disciplina.codigo %></label>
            
            <asp:Label runat="server" Text="Período:"></asp:Label>
            <label><%: lista.periodo %></label>
            
            <asp:Label runat="server" Text="Nota do aluno:"></asp:Label>
            <label><%: lista.nota %></label>
            
            <asp:Label runat="server" Text="Data da nota:"></asp:Label>
            <label><%: lista.data %></label>
            
            <asp:Label runat="server" Text="Origem da noda:"></asp:Label>
            <label><%: lista.origem %></label>

        <% } %>
    </div>
    </div>
</asp:Content>
