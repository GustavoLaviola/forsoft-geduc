﻿using geducSite.Models;
using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace geducSite.View
{
    public partial class alterar_responsavel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    ProgramaSocialDAO psd = new ProgramaSocialDAO();
                    DataTable dtp = psd.preencherDropPrograma();
                    ddlProgramaSocial.DataSource = dtp;
                    ddlProgramaSocial.DataValueField = "idProgramaSocial";
                    ddlProgramaSocial.DataTextField = "nomePrograma";
                    ddlProgramaSocial.DataBind();

                }
                catch (Exception ex)
                {
                    msg.Text = ex.Message;
                }

            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Responsavel r = new Responsavel();
                ResponsavelDAO rd = new ResponsavelDAO();

                r = rd.buscarResponsavelPorCpf(txtBuscar.Text);

                //Login
                txtUsuario.Text = r.login.usuario;
                txtSenha.Text = r.login.senha;

                //Pessoais
                txtNomeresponsavel.Text = r.nome;
                txtDataNascimento.Text = r.dataNascimento.ToString();
                rbSexo.SelectedValue = r.sexo;
                txtNaturalidade.Text = r.naturalidade;
                txtNacionalidade.Text = r.nacionalidade;
                txtNomePai.Text = r.nomePai;
                txtNomeMae.Text = r.nomeMae;
                ddlEtnia.SelectedValue = r.etnia;
                ddlEstadoCivil.SelectedValue = r.estadoCivil;
                ddlEscolaridade.SelectedValue = r.nivelEscolaridade;
                rbNecessidadeEspecial.SelectedValue = r.necessidadeEsp;

                //Documento
                txtCPF.Text = r.documento.cpf;
                lblCpfAtual.Text = r.documento.cpf;
                txtRG.Text = r.documento.rg;
                txtDataExpedicao.Text = r.documento.dataExpedicao.ToString();
                txtOrgao.Text = r.documento.orgaoExpedidor;
                txtNumCertidaoNascimento.Text = r.documento.numCertidao;
                txtLivroCertidaoNascimento.Text = r.documento.livroCertidao;
                txtFolhaCertidaoNascimento.Text = r.documento.folhaCertidao;
                txtDataCertidaoNascimento.Text = r.documento.dataEmiCertidao.ToString();
                txtTituloEleitor.Text = r.documento.titEleitor;
                txtCertificadoReservista.Text = r.documento.certReservista;

                //Endereco
                txtLogradouro.Text = r.endereco.longradouro;
                txtNumero.Text = r.endereco.numero;
                txtComplemento.Text = r.endereco.complemento;
                txtBairro.Text = r.endereco.bairro;
                txtCidade.Text = r.endereco.cidade;
                ddlUF.SelectedValue = r.endereco.uf;
                txtCEP.Text = r.endereco.cep;
                txtMunicipio.Text = r.endereco.municipio;
                txtZona.Text = r.endereco.zona;

                //Contato
                txtTelefone.Text = r.contato.telefoneFixo;
                txtCelular.Text = r.contato.telefoneCelular;
                txtEmail.Text = r.contato.email;
                txtOutros.Text = r.contato.outros;

                //Programa Social
                ddlProgramaSocial.SelectedValue = r.programaSocial.idProgramaSocial.ToString();
            }
            catch (Exception ex)
            {
                msg.Text = ex.Message;
            }
        }

        protected void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                Responsavel r = new Responsavel();
                r.contato = new Contato();
                r.endereco = new Endereco();
                r.login = new Models.Login();
                r.programaSocial = new ProgramaSocial();
                r.documento = new Documento();

                ResponsavelDAO rd = new ResponsavelDAO();

                //Login
                r.login.usuario = txtUsuario.Text;
                r.login.senha = txtSenha.Text;

                //Dados Pessoais
                r.nome = txtNomeresponsavel.Text;
                r.dataNascimento = Convert.ToDateTime(txtDataNascimento.Text);
                r.sexo = rbSexo.SelectedValue;
                r.naturalidade = txtNaturalidade.Text;
                r.nacionalidade = txtNacionalidade.Text;
                r.nomePai = txtNomePai.Text;
                r.nomeMae = txtNomeMae.Text;
                r.etnia = ddlEtnia.SelectedValue;
                r.estadoCivil = ddlEstadoCivil.SelectedValue;
                r.nivelEscolaridade = ddlEscolaridade.SelectedValue;
                r.necessidadeEsp = rbNecessidadeEspecial.SelectedValue;

                //Documentação
                r.documento.cpf = txtCPF.Text;
                r.documento.rg = txtRG.Text;
                r.documento.dataExpedicao = Convert.ToDateTime(txtDataExpedicao.Text);
                r.documento.orgaoExpedidor = txtOrgao.Text;
                r.documento.numCertidao = txtNumCertidaoNascimento.Text;
                r.documento.livroCertidao = txtLivroCertidaoNascimento.Text;
                r.documento.folhaCertidao = txtFolhaCertidaoNascimento.Text;
                r.documento.dataEmiCertidao = Convert.ToDateTime(txtDataCertidaoNascimento.Text);
                r.documento.titEleitor = txtTituloEleitor.Text;
                r.documento.certReservista = txtCertificadoReservista.Text;

                //Endereco
                r.endereco.longradouro = txtLogradouro.Text;
                r.endereco.numero = txtNumero.Text;
                r.endereco.complemento = txtComplemento.Text;
                r.endereco.bairro = txtBairro.Text;
                r.endereco.cidade = txtCidade.Text;
                r.endereco.uf = ddlUF.SelectedValue;
                r.endereco.cep = txtCEP.Text;
                r.endereco.municipio = txtMunicipio.Text;
                r.endereco.zona = txtZona.Text;

                //Contato
                r.contato.telefoneFixo = txtTelefone.Text;
                r.contato.telefoneCelular = txtCelular.Text;
                r.contato.outros = txtOutros.Text;
                r.contato.email = txtEmail.Text;

                //Programa Social
                r.programaSocial.idProgramaSocial = Convert.ToInt32(ddlProgramaSocial.SelectedValue);

                //Enviar objeto responsavel para ser salvo no banco
                rd.AlterarResponsavel(r, lblCpfAtual.Text);

                //Mensagem de sucesso
                msg.Text = "Osdados do responsável " + r.nome + ", foram atualizados com sucesso!";
            }
            catch (Exception ex)
            {
                msg.Text = ex.Message;
            }
        }
    }
}