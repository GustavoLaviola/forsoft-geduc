﻿using geducSite.Models;
using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace geducSite.View
{
    public partial class alterar_aluno : System.Web.UI.Page
    {
        protected IEnumerable<Aluno> todosOsAlunos;
        protected Aluno retorno;

        protected void Page_Load(object sender, EventArgs e)
        {
            /*
            geducSite.Models.Login login = (geducSite.Models.Login)Session["login"];

            if (login == null)
            {
                if (Request.Cookies["login"] != null)
                {
                    HttpCookie coockie = Request.Cookies["login"];
                    string ck = coockie.Value.ToString();

                    Session["login"] = new UtilDAO().SessionLogin(ck);
                }
                else
                {
                    Response.Redirect("http://www.projetogeduc.com.br:8080/geduc/login.jsp");
                }
            }
            else if (Util.ValidarAcesso(login.perfilAcesso, "alterar_aluno.aspx") == false)
            {
                Response.Redirect("http://www.projetojeduc.com.br:8080/geduc/nao_pode_acessar.jsp");
            }
             * */
            //fim session login


            todosOsAlunos = new AlunoDAO().ListarAluno();

            if (!Page.IsPostBack)
            {
                try
                {

                    ProgramaSocialDAO psd = new ProgramaSocialDAO();
                    DataTable dtp = psd.preencherDropPrograma();
                    ddlProgramaSocial.DataSource = dtp;
                    ddlProgramaSocial.DataValueField = "idProgramaSocial";
                    ddlProgramaSocial.DataTextField = "nomePrograma";
                    ddlProgramaSocial.DataBind();

                    CursoDAO cd = new CursoDAO();
                    DataTable dtc = cd.preencherDropCurso();
                    ddlCurso.DataSource = dtc;
                    ddlCurso.DataValueField = "idCurso";
                    ddlCurso.DataTextField = "nomeC";
                    ddlCurso.DataBind();

                }
                catch (Exception ex)
                {
                    msg.Text = ex.Message;
                }

            }

        }

        protected void buscarAlunos()
        {
            int valor = Convert.ToInt32(ddlTipoDeBusca.SelectedValue);
            switch (valor)
            {
                case 1:
                    retorno = todosOsAlunos.SingleOrDefault(x => x.documento.cpf == txtBuscar.Text);
                    if (retorno != null)
                    {
                        preencher(retorno);
                    }
                    else
                    {
                        msg.Text = "O aluno nao foi encontrado";
                    }
                    break;

                case 2:
                    retorno = todosOsAlunos.SingleOrDefault(x => x.matricula == txtBuscar.Text);
                    if (retorno != null)
                        preencher(retorno);

                    break;
            }
        }

        private void preencher(Aluno a)
        {

            //Login
            txtUsuario.Text = a.login.usuario;
            txtSenha.Text = a.login.senha;

            //Dados Pessoais
            txtNomeAluno.Text = a.nome;
            txtDataCertidaoNascimento.Text = Convert.ToString(a.dataNascimento);
            txtNaturalidade.Text = a.naturalidade;
            txtNacionalidade.Text = a.nacionalidade;
            txtNomePai.Text = a.nomePai;
            txtNomeMae.Text = a.nomeMae;
            ddlEtnia.SelectedValue = a.etnia;
            ddlEstadoCivil.SelectedValue = a.estadoCivil;
            ddlEscolaridade.SelectedValue = a.nivelEscolaridade;

            //Dados de Aluno
            ddlSituacao.SelectedValue = a.situacao;
            txtMatricula.Text = a.matricula;

            //Documentação
            txtCPF.Text = a.documento.cpf;
            txtDataExpedicao.Text = Convert.ToString(a.documento.dataExpedicao);
            txtDataCertidaoNascimento.Text = Convert.ToString(a.documento.dataEmiCertidao);
            txtRG.Text = a.documento.rg;
            txtOrgao.Text = a.documento.orgaoExpedidor;
            txtNumCertidaoNascimento.Text = a.documento.numCertidao;
            txtLivroCertidaoNascimento.Text = a.documento.livroCertidao;
            txtFolhaCertidaoNascimento.Text = a.documento.folhaCertidao;
            txtTituloEleitor.Text = a.documento.titEleitor;
            txtCertificadoReservista.Text = a.documento.certReservista;

            //Endereco
            txtLogradouro.Text = a.endereco.longradouro;
            txtNumero.Text = Convert.ToString(a.endereco.numero);
            txtComplemento.Text = a.endereco.complemento;
            txtBairro.Text = a.endereco.bairro;
            txtCidade.Text = a.endereco.cidade;
            ddlUF.SelectedValue = a.endereco.uf;
            txtCEP.Text = a.endereco.cep;
            txtMunicipio.Text = a.endereco.municipio;
            txtZona.Text = a.endereco.zona;

            //Contato
            txtTelefone.Text = a.contato.telefoneFixo;
            txtCelular.Text = a.contato.telefoneCelular;
            txtOutros.Text = a.contato.outros;
            txtEmail.Text = a.contato.email;

            //Curso
            ddlCurso.SelectedValue = Convert.ToString(a.curso.idCurso);

            //Programa Social
            ddlProgramaSocial.SelectedValue = Convert.ToString(a.programaSocial.idProgramaSocial);
        }


        protected void btnAlterar_Click(object sender, EventArgs e)
        {
            msg.Text = String.Empty;

            String[] campos = new String[] { txtUsuario.Text, txtSenha.Text, txtNomeAluno.Text, txtDataNascimento.Text, rbSexo.SelectedValue,
                txtNacionalidade.Text, ddlEscolaridade.SelectedValue, ddlEtnia.SelectedValue, txtEmail.Text, ddlEstadoCivil.SelectedValue,
                txtLogradouro.Text, txtNumero.Text, txtBairro.Text, txtCidade.Text, ddlUF.SelectedValue, txtCEP.Text, txtMunicipio.Text };

            String[] camposSomenteNumero = new String[] { txtCelular.Text, txtTelefone.Text, txtCEP.Text };
            String[] camposSomenteLetras = new String[] { txtNomeAluno.Text, txtNaturalidade.Text, txtNacionalidade.Text, txtNomePai.Text,
                txtNomeMae.Text, txtCidade.Text, txtMunicipio.Text };
            String[] camposData = new String[] { txtDataNascimento.Text, txtDataExpedicao.Text, txtDataCertidaoNascimento.Text };

            String[] campos255 = new String[] { txtUsuario.Text, txtSenha.Text, txtMatricula.Text, txtLogradouro.Text, txtBairro.Text };
            String[] campos200 = new String[] { txtNomePai.Text, txtNomeMae.Text, txtNumCertidaoNascimento.Text, txtLivroCertidaoNascimento.Text,
                txtFolhaCertidaoNascimento.Text, txtCertificadoReservista.Text, txtEmail.Text };
            String[] campos100 = new String[] { txtNacionalidade.Text, txtOrgao.Text, txtTituloEleitor.Text };
            String[] campos40 = new String[] { txtNumero.Text, txtComplemento.Text, txtCidade.Text, ddlUF.SelectedValue, txtMunicipio.Text,
                txtZona.Text };
            String[] campos30 = new String[] { ddlEscolaridade.SelectedValue, ddlSituacao.SelectedValue };
            String[] campos20 = new String[] { txtNaturalidade.Text, rbSexo.SelectedValue, ddlEtnia.SelectedValue,
                rbNecessidadeEspecial.SelectedValue, ddlEstadoCivil.SelectedValue, txtTelefone.Text, txtCelular.Text };

            if (Validador.tamanhoMax(campos20, 20) && Validador.tamanhoMax(campos200, 200) && Validar.tamanhoMax(txtNomeAluno.Text, 150) &&
                Validador.tamanhoMax(campos100, 100) && Validador.tamanhoMax(campos30, 30) && Validador.tamanhoMax(campos255, 255) &&
                Validador.tamanhoMax(campos40, 40))
            {
                if (Validador.seSomenteLetra(camposSomenteLetras) && Validador.seSomenteNumero(camposSomenteNumero) && Validador.seData(camposData)
                && Validar.seCEP(txtCEP.Text) && Validar.seCPF(txtCPF.Text) && Validar.seEmail(txtEmail.Text))
                {
                    try
                    {
                        Aluno a = todosOsAlunos.SingleOrDefault(x => x.matricula == txtMatricula.Text);

                        if (a != null)
                        {
                            //Login
                            a.login.usuario = txtUsuario.Text;
                            a.login.senha = txtSenha.Text;
                            a.login.perfilAcesso = "Aluno";

                            //Dados Pessoais
                            a.nome = txtNomeAluno.Text;
                            a.dataNascimento = Convert.ToDateTime(txtDataNascimento.Text);
                            a.sexo = rbSexo.SelectedValue;
                            a.naturalidade = txtNaturalidade.Text;
                            a.nacionalidade = txtNacionalidade.Text;
                            a.nomePai = txtNomePai.Text;
                            a.nomeMae = txtNomeMae.Text;
                            a.etnia = ddlEtnia.SelectedValue;
                            a.estadoCivil = ddlEstadoCivil.SelectedValue;
                            a.nivelEscolaridade = ddlEscolaridade.SelectedValue;
                            a.necessidadeEsp = rbNecessidadeEspecial.SelectedValue;

                            //Dados de Aluno
                            a.situacao = ddlSituacao.SelectedValue;
                            a.matricula = txtMatricula.Text;

                            //Documentação
                            a.documento.cpf = txtCPF.Text;
                            a.documento.rg = txtRG.Text;
                            a.documento.dataExpedicao = Convert.ToDateTime(txtDataExpedicao.Text);
                            a.documento.orgaoExpedidor = txtOrgao.Text;
                            a.documento.numCertidao = txtNumCertidaoNascimento.Text;
                            a.documento.livroCertidao = txtLivroCertidaoNascimento.Text;
                            a.documento.folhaCertidao = txtFolhaCertidaoNascimento.Text;
                            a.documento.dataEmiCertidao = Convert.ToDateTime(txtDataCertidaoNascimento.Text);
                            a.documento.titEleitor = txtTituloEleitor.Text;
                            a.documento.certReservista = txtCertificadoReservista.Text;

                            //Endereco
                            a.endereco.longradouro = txtLogradouro.Text;
                            a.endereco.numero = txtNumero.Text;
                            a.endereco.complemento = txtComplemento.Text;
                            a.endereco.bairro = txtBairro.Text;
                            a.endereco.cidade = txtCidade.Text;
                            a.endereco.uf = ddlUF.SelectedValue;
                            a.endereco.cep = txtCEP.Text;
                            a.endereco.municipio = txtMunicipio.Text;
                            a.endereco.zona = txtZona.Text;

                            //Contato
                            a.contato.telefoneFixo = txtTelefone.Text;
                            a.contato.telefoneCelular = txtCelular.Text;
                            a.contato.outros = txtOutros.Text;
                            a.contato.email = txtEmail.Text;

                            //Curso
                            a.curso.idCurso = Convert.ToInt32(ddlCurso.SelectedValue);

                            //Programa Social
                            a.programaSocial.idProgramaSocial = Convert.ToInt32(ddlProgramaSocial.SelectedValue);

                            //Salvando o Aluno
                            new AlunoDAO().Alterar(a);

                            limparCampos();

                            msg.Text = "O aluno " + a.nome + ", foi cadastrado com sucesso!";
                        }
                        else msg.Text = "O aluno não foi encontrado";
                    }
                    catch (Exception ex)
                    {
                        msg.Text = ex.Message;
                    }
                }
                else msg.Text = "Preenchimento de campos inválido.";
            }
            else msg.Text = "Ultrapassou o limite máximo de caracteres";
        }

        private void limparCampos()
        {
            //Login
            txtUsuario.Text = null;
            txtSenha.Text = null;

            //Dados Pessoais
            txtNomeAluno.Text = null;
            txtNaturalidade.Text = null;
            txtNacionalidade.Text = null;
            txtNomePai.Text = null;
            txtNomeMae.Text = null;
            ddlEtnia.SelectedIndex = 0;
            ddlEstadoCivil.SelectedIndex = 0;
            ddlEscolaridade.SelectedIndex = 0;

            //Dados de Aluno
            ddlSituacao.SelectedIndex = 0;
            txtMatricula.Text = null;

            //Documentação
            txtCPF.Text = null;
            txtRG.Text = null;
            txtOrgao.Text = null;
            txtNumCertidaoNascimento.Text = null;
            txtLivroCertidaoNascimento.Text = null;
            txtFolhaCertidaoNascimento.Text = null;
            txtTituloEleitor.Text = null;
            txtCertificadoReservista.Text = null;

            //Endereco
            txtLogradouro.Text = null;
            txtNumero.Text = null;
            txtComplemento.Text = null;
            txtBairro.Text = null;
            txtCidade.Text = null;
            ddlUF.SelectedIndex = 0;
            txtCEP.Text = null;
            txtMunicipio.Text = null;
            txtZona.Text = null;

            //Contato
            txtTelefone.Text = null;
            txtCelular.Text = null;
            txtOutros.Text = null;
            txtEmail.Text = null;

            //Curso
            ddlCurso.SelectedIndex = 0;

            //Programa Social
            ddlProgramaSocial.SelectedIndex = 0;
        }

        protected void btnLimpar_Click(object sender, EventArgs e)
        {
            limparCampos();
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            buscarAlunos();
        }
    }
}